/*
 * GStreamer gstreamer-tfliteclient
 * Copyright (C) 2024 Collabora Ltd
 *
 * gsttfliteclient.h
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA. *
 *
 * Inspired by:
 * Author: Vincent Abriou <vincent.abriou@st.com> for STMicroelectronics.
 *
 * Copyright (c) 2020 STMicroelectronics. All rights reserved.
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 *     http://www.opensource.org/licenses/BSD-3-Clause
 *
 *
 *
 * Inspired by:
 * https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/examples/label_image
 * Copyright 2017 The TensorFlow Authors. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#ifndef __GST_TFLITE_CLIENT_H__
#define __GST_TFLITE_CLIENT_H__

#include <gst/gst.h>

#include "tensorflow/lite/kernels/register.h"
#include "tensorflow/lite/model.h"
#include "tensorflow/lite/optional_debug_tools.h"

#ifdef EDGETPU
#include "tflite/public/edgetpu.h"
#endif

#include "tensorflow/lite/delegates/external/external_delegate.h"
#include "tensorflow/lite/interpreter.h"

#include "VX/vsi_npu_custom_op.h"

#include <gst/video/video.h>
#include "gstml.h"
#include "tensor/gsttensormeta.h"

GST_DEBUG_CATEGORY_EXTERN (tflite_inference_debug);

typedef enum
{
  GST_TFLITE_EXECUTION_PROVIDER_CPU,
  GST_TFLITE_EXECUTION_PROVIDER_NPU,
  GST_TFLITE_EXECUTION_PROVIDER_EDGE_TPU,
} GstTFliteExecutionProvider;

#define DEFAULT_MODEL_FILE              ""
#define DEFAULT_INPUT_IMAGE_FORMAT      GST_ML_INPUT_IMAGE_FORMAT_HWC
#define DEFAULT_EXECUTION_PROVIDER      GST_TFLITE_EXECUTION_PROVIDER_CPU
#define DEFAULT_INPUT_INDEX             0
#define DEFAULT_EXT_DELEGATE_LIB_PATH   ""
#define DEFAULT_THREADS                 2
#define DEFAULT_INPUT_STD               0.0
#define DEFAULT_INPUT_MEAN              0.0

namespace GstTFliteNamespace {
	struct Config {
		bool verbose;
		float input_mean = 127.5f;
		float input_std = 127.5f;
		int number_of_threads = 2;
		int number_of_results = 5;
		std::string model_name;
		std::string labels_file_name;
		bool edgetpu;
		bool accel;
		std::string external_delegate_path;
	};

  class GstTFliteClient {
  public:
    GstTFliteClient(GstElement *debug_parent);
    ~GstTFliteClient(void);
    bool createSession(std::string modelFile,
      GstTFliteExecutionProvider provider);
    bool hasSession(void);
    void setInputImageFormat(GstMlInputImageFormat format);
    void setInputIndex (int index);
    int getInputIndex (void);
    int getOutputSize (int index);
    GstMlInputImageFormat getInputImageFormat(void);
    GstTensorDataType getInputImageDatatype(void);
    GstTensorDataType getOutputDatatype (size_t index);
    int setThreads (int n_threads);
    int getThreads ();
    void setInputStd (float std_value);
    float getInputStd ();
    void setInputMean (float mean_value);
    float getInputMean ();
    void setExtDelegatePath (const gchar * path);
    const gchar* getExtDelegatePath() const;
    bool run (uint8_t * img_data, GstVideoInfo vinfo);
    std::vector < const char *> genOutputNamesRaw(void);
    bool isFixedInputImageSize(void);
    int32_t getWidth(void);
    int32_t getHeight(void);
    int32_t getChannels (void);
    GstTensorMeta * copy_tensors_to_meta (GstBuffer * buffer);
    void parseDimensions(GstVideoInfo vinfo);
  private:
    template < typename T>
    void convert_image_remove_alpha (T *dest, GstMlInputImageFormat hwc,
        uint8_t **srcPtr, uint32_t srcSamplesPerPixel, uint32_t stride, T offset, T div);
    GstElement *debug_parent;
#ifdef EDGETPU
		std::shared_ptr<edgetpu::EdgeTpuContext> m_edgetpu_ctx;
#endif
    uint8_t *dest;
		std::unique_ptr<tflite::FlatBufferModel> m_model;
		std::unique_ptr<tflite::Interpreter>     m_interpreter;
		bool                                     m_verbose;
		bool                                     m_inputFloating;
		bool                                     m_allow_fp16;
		float                                    m_inputMean;
		float                                    m_inputStd;
		float                                    m_inferenceTime;
		int                                      m_inputIndex;
		int                                      m_numberOfThreads;
		int                                      m_numberOfResults;
		bool                                     m_edgetpu;
		bool                                     m_accel;
		bool                                     m_npu;
		gchar *                                  m_external_delegate_path;
		bool                                     m_fixedInputImageSize;
		std::string                              m_vxdelegate;
    GstMlInputImageFormat                    m_inputImageFormat;
    size_t                                   inputDatatypeSize;
    float                                    inputTensorScale;
  };
}

#endif                          /* __GST_TFLITE_CLIENT_H__ */
