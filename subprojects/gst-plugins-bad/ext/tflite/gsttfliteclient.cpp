/*
 * GStreamer gstreamer-tfliteclient
 * Copyright (C) 2024 Collabora Ltd
 *
 * gsttfliteclient.cpp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA. *
 *
 * Inspired by:
 * Author: Vincent Abriou <vincent.abriou@st.com> for STMicroelectronics.
 *
 * Copyright (c) 2020 STMicroelectronics. All rights reserved.
 *
 * This software component is licensed by ST under BSD 3-Clause license,
 * the "License"; You may not use this file except in compliance with the
 * License. You may obtain a copy of the License at:
 *
 *     http://www.opensource.org/licenses/BSD-3-Clause
 *
 *
 *
 * Inspired by:
 * https://github.com/tensorflow/tensorflow/tree/master/tensorflow/lite/examples/label_image
 * Copyright 2017 The TensorFlow Authors. All Rights Reserved.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * You may obtain a copy of the License at:
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 */

#include "gsttfliteclient.h"
#include <sstream>

#define GST_CAT_DEFAULT tflite_inference_debug

namespace GstTFliteNamespace
{
  template < typename T >
      std::ostream & operator<< (std::ostream & os, const std::vector < T > &v)
  {
    os << "[";
    for (size_t i = 0; i < v.size (); ++i)
    {
      os << v[i];
      if (i != v.size () - 1)
      {
        os << ", ";
      }
    }
    os << "]";

    return os;
  }

  GstTFliteClient::GstTFliteClient (GstElement * debug_parent):
    debug_parent (debug_parent), m_inputMean (DEFAULT_INPUT_MEAN),
    m_inputStd (DEFAULT_INPUT_STD), m_inputIndex (DEFAULT_INPUT_INDEX),
    m_numberOfThreads (DEFAULT_THREADS),
    m_fixedInputImageSize (true), m_inputImageFormat (DEFAULT_INPUT_IMAGE_FORMAT){
    m_external_delegate_path = g_strdup("");
  }

  GstTFliteClient::~GstTFliteClient () {
    if(!dest)
      delete[]dest;
    g_free(m_external_delegate_path);
  }

  int32_t GstTFliteClient::getWidth (void)
  {
    int input = m_interpreter->inputs ()[m_inputIndex];
    TfLiteIntArray *input_dims = m_interpreter->tensor (input)->dims;
    return input_dims->data[2];
  }

  int32_t GstTFliteClient::getHeight (void)
  {
    int input = m_interpreter->inputs ()[m_inputIndex];
    TfLiteIntArray *input_dims = m_interpreter->tensor (input)->dims;
    return input_dims->data[1];
  }

  int32_t GstTFliteClient::getChannels (void)
  {
    int input = m_interpreter->inputs ()[m_inputIndex];
    TfLiteIntArray *input_dims = m_interpreter->tensor (input)->dims;
    return input_dims->data[3];
  }

  bool GstTFliteClient::isFixedInputImageSize (void)
  {
    return m_fixedInputImageSize;
  }

  void GstTFliteClient::setInputImageFormat (GstMlInputImageFormat format)
  {
    m_inputImageFormat = format;
  }

  GstMlInputImageFormat GstTFliteClient::getInputImageFormat (void)
  {
    return m_inputImageFormat;
  }

  void GstTFliteClient::setInputIndex (int index)
  {
    m_inputIndex = index;
  }
  int GstTFliteClient::getInputIndex (void)
  {
    return m_inputIndex;
  }

  int GstTFliteClient::getOutputSize (int index)
  {
    int output = m_interpreter->outputs()[index];
    TfLiteIntArray* output_dims = m_interpreter->tensor(output)->dims;
    // assume output dims to be something like (1, 1, ... ,size)
    return output_dims->data[output_dims->size - 1];
  }

  int GstTFliteClient::setThreads (int n_threads)
  {
    m_numberOfThreads = n_threads;
  }

  int GstTFliteClient::getThreads ()
  {
    return m_numberOfThreads;
  }

  void GstTFliteClient::setInputStd (float std_value)
  {
    m_inputStd = std_value;
  }

  float GstTFliteClient::getInputStd ()
  {
    return m_inputStd;
  }

  void GstTFliteClient::setInputMean (float mean_value)
  {
    m_inputMean = mean_value;
  }

  float GstTFliteClient::getInputMean ()
  {
    return m_inputMean;
  }

  void GstTFliteClient::setExtDelegatePath (const gchar * path)
  {
    if (m_external_delegate_path)
      g_free (m_external_delegate_path);
    m_external_delegate_path = g_strdup(path);
  }

  const gchar* GstTFliteClient::getExtDelegatePath() const {
      return m_external_delegate_path;
  }

  GstTensorDataType GstTFliteClient::getInputImageDatatype (void)
  {
    int input = m_interpreter->inputs()[0];
    switch (m_interpreter->tensor (input)->type){
      case kTfLiteFloat32:
        inputDatatypeSize = sizeof (float);
        return GST_TENSOR_TYPE_FLOAT32;
      case kTfLiteInt32:
        inputDatatypeSize = sizeof (int32_t);
        return GST_TENSOR_TYPE_INT32;
      case kTfLiteUInt8:
        inputDatatypeSize = sizeof (uint8_t);
        return GST_TENSOR_TYPE_UINT8;
      case kTfLiteInt64:
        inputDatatypeSize = sizeof (int64_t);
        return GST_TENSOR_TYPE_INT64;
      case kTfLiteBool:
        inputDatatypeSize = sizeof (bool);
        return GST_TENSOR_TYPE_UINT8;
      case kTfLiteInt16:
        inputDatatypeSize = sizeof (int16_t);
        return GST_TENSOR_TYPE_INT16;
      case kTfLiteInt8:
        inputDatatypeSize = sizeof (uint8_t);
        return GST_TENSOR_TYPE_INT8;
      // Cases not supported
      case kTfLiteComplex64:
      case kTfLiteString:
      case kTfLiteNoType:
      default:
        GST_FIXME_OBJECT (debug_parent,          "GstTensorDataType currently does not have a mapping \
          for this type. Returns GST_TENSOR_TYPE_INT32 for now.");
        g_error ("Data type %d not handled", m_interpreter->tensor (input)->type);
        return GST_TENSOR_TYPE_INT32;
    }
  }

  GstTensorDataType GstTFliteClient::getOutputDatatype (size_t index)
  {
    TfLiteType tflite_output_type = m_interpreter->output_tensor(index)->type;
    switch (tflite_output_type){
      case kTfLiteFloat32:
        inputDatatypeSize = sizeof (float);
        return GST_TENSOR_TYPE_FLOAT32;
      case kTfLiteInt32:
        inputDatatypeSize = sizeof (int32_t);
        return GST_TENSOR_TYPE_INT32;
      case kTfLiteUInt8:
        inputDatatypeSize = sizeof (uint8_t);
        return GST_TENSOR_TYPE_UINT8;
      case kTfLiteInt64:
        inputDatatypeSize = sizeof (int64_t);
        return GST_TENSOR_TYPE_INT64;
      case kTfLiteBool:
        inputDatatypeSize = sizeof (bool);
        return GST_TENSOR_TYPE_UINT8;
      case kTfLiteInt16:
        inputDatatypeSize = sizeof (int16_t);
        return GST_TENSOR_TYPE_INT16;
      case kTfLiteInt8:
        inputDatatypeSize = sizeof (uint8_t);
        return GST_TENSOR_TYPE_INT8;
      // Cases not supported
      case kTfLiteComplex64:
      case kTfLiteString:
      case kTfLiteNoType:
      default:
        GST_FIXME_OBJECT (debug_parent,
          "GstTensorDataType currently does not have a mapping \
          for this type. Returns GST_TENSOR_TYPE_INT32 for now.");
        g_error ("Data type %d not handled", tflite_output_type);
        return GST_TENSOR_TYPE_INT32;
    }
  }

  bool GstTFliteClient::hasSession (void)
  {
    return m_interpreter.get() != nullptr;
  }

  bool GstTFliteClient::createSession (std::string modelFile, GstTFliteExecutionProvider provider)
  {
    m_inputFloating = false;
    m_allow_fp16 = false;
    m_inferenceTime = 0;
    bool m_vx = false;

    switch (provider) {
      case GST_TFLITE_EXECUTION_PROVIDER_NPU:
        m_accel = true;
        m_edgetpu = false;
        /*  Check if vx delegate is used  */
        if (g_strstr_len(m_external_delegate_path, -1,"libvx_delegate") != NULL)
            m_vx = true;
        break;
      case GST_TFLITE_EXECUTION_PROVIDER_EDGE_TPU:
        m_accel = false;
        m_edgetpu = true;
        break;
      default:
      case GST_TFLITE_EXECUTION_PROVIDER_CPU:
        m_accel = false;
        m_edgetpu = false;
        break;
    }

    if (m_edgetpu) {
      /*  Check if the Edge TPU is connected */
      int status = system ("lsusb -d 1a6e:");
      status &= system ("lsusb -d 18d1:");
      if (status) {
        GST_ERROR_OBJECT (debug_parent, "ERROR: Edge TPU not connected.");
        return false;
      }
      /* Load EDGETPU */
#ifdef EDGETPU
      m_edgetpu_ctx = edgetpu::EdgeTpuManager::GetSingleton ()->OpenDevice ();
#endif
    }

    if (!modelFile.c_str ()) {
      GST_ERROR_OBJECT (debug_parent, "no model file name");
      return false;
    }

    std::unique_ptr < tflite::FlatBufferModel > model;
    std::unique_ptr < tflite::Interpreter > interpreter;
    model = tflite::FlatBufferModel::BuildFromFile (modelFile.c_str ());
    if (!model) {
      GST_ERROR_OBJECT (debug_parent, "Failed to mmap model %s",
          modelFile.c_str ());
      return false;
    }
    GST_DEBUG_OBJECT (debug_parent, "Loaded model %s", modelFile.c_str ());
    model->error_reporter ();

    tflite::ops::builtin::BuiltinOpResolver resolver;

    if (m_edgetpu) {
#ifdef EDGETPU
      resolver.AddCustom (edgetpu::kCustomOp, edgetpu::RegisterCustomOp ());
#endif
    }
    if (m_accel && m_vx) {
      resolver.AddCustom (kNbgCustomOp,
          tflite::ops::custom::Register_VSI_NPU_PRECOMPILED ());
    }

    tflite::InterpreterBuilder (*model, resolver) (&interpreter);
    if (!interpreter) {
      GST_ERROR_OBJECT (debug_parent, "Failed to construct interpreter");
      return false;
    }
    if (m_accel) {
      const char *delegate_path = m_external_delegate_path;
      auto ext_delegate_option =
          TfLiteExternalDelegateOptionsDefault (delegate_path);
      auto ext_delegate_ptr =
          TfLiteExternalDelegateCreate (&ext_delegate_option);
      interpreter->ModifyGraphWithDelegate (ext_delegate_ptr);
    }

    int input = interpreter->inputs ()[0];
    if (interpreter->tensor (input)->type == kTfLiteFloat32) {
      m_inputFloating = true;
      GST_DEBUG_OBJECT (debug_parent, "Floating point Tensorflow Lite Model");
    }

    if (m_edgetpu) {
#ifdef EDGETPU
      interpreter->SetExternalContext (kTfLiteEdgeTpuContext,
          m_edgetpu_ctx.get ());
#endif
    } else {
      interpreter->SetAllowFp16PrecisionForFp32 (m_allow_fp16);
    }

    if (m_numberOfThreads != -1) {
      interpreter->SetNumThreads (m_numberOfThreads);
    }

    m_interpreter = std::move (interpreter);
    m_model = std::move (model);

    return true;
  }

/* Object detection tensor id strings */
#define GST_MODEL_OBJECT_DETECTOR_BOXES "Gst.Model.ObjectDetector.Boxes"
#define GST_MODEL_OBJECT_DETECTOR_SCORES "Gst.Model.ObjectDetector.Scores"
#define GST_MODEL_OBJECT_DETECTOR_NUM_DETECTIONS "Gst.Model.ObjectDetector.NumDetections"
#define GST_MODEL_OBJECT_DETECTOR_CLASSES "Gst.Model.ObjectDetector.Classes"

  // copy tensor data to a GstTensorMeta
  GstTensorMeta * GstTFliteClient::copy_tensors_to_meta (GstBuffer * buffer)
  {
    size_t num_tensors = m_interpreter->outputs().size();
    GstTensorMeta *tmeta = (GstTensorMeta *) gst_buffer_add_meta (buffer,
        gst_tensor_meta_get_info (), NULL);
    auto my_meta = gst_tensor_meta_api_get_type();
    tmeta->num_tensors = num_tensors;
    tmeta->tensor = (GstTensor *) g_malloc (num_tensors * sizeof (GstTensor));

    for (size_t i = 0; i < num_tensors; i++) {

      TfLiteTensor * output_tensor = m_interpreter->output_tensor(i);
      GstTensor *meta_tensor = &tmeta->tensor[i];

      meta_tensor->data_type = getOutputDatatype(i);
      if (i==0)
        meta_tensor->id = g_quark_from_static_string (GST_MODEL_OBJECT_DETECTOR_BOXES);
      if (i==1)
        meta_tensor->id = g_quark_from_static_string (GST_MODEL_OBJECT_DETECTOR_CLASSES);
      if (i==2)
        meta_tensor->id = g_quark_from_static_string (GST_MODEL_OBJECT_DETECTOR_SCORES);
      if (i==3)
        meta_tensor->id = g_quark_from_static_string (GST_MODEL_OBJECT_DETECTOR_NUM_DETECTIONS);

      meta_tensor->num_dims = output_tensor->dims->size;
      meta_tensor->dims = g_new (int64_t, meta_tensor->num_dims);

      for (int j = 0; j < meta_tensor->num_dims; ++j)
        meta_tensor->dims[j] = output_tensor->dims->data[j];

      size_t buffer_size = output_tensor->bytes;

      if (meta_tensor->data_type == GST_TENSOR_TYPE_FLOAT32) {
        meta_tensor->data = gst_buffer_new_allocate (NULL, buffer_size, NULL);
        gst_buffer_fill (meta_tensor->data, 0,
          m_interpreter->typed_output_tensor<float>(i), buffer_size);
      } else if (meta_tensor->data_type == GST_TENSOR_TYPE_INT32) {
        meta_tensor->data = gst_buffer_new_allocate (NULL, buffer_size, NULL);
        gst_buffer_fill (meta_tensor->data, 0,
          m_interpreter->typed_output_tensor<int32_t>(i), buffer_size);
      } else {
        GST_ERROR_OBJECT (debug_parent, "Output tensor is not FLOAT32 or INT32, not supported");
        gst_buffer_remove_meta (buffer, (GstMeta*) tmeta);
        return NULL;
      }
    }

    return tmeta;
  }

  bool GstTFliteClient::run (uint8_t * img_data, GstVideoInfo vinfo) {
    if (!img_data){
      return false;
    }
    /* Parse dimensions of incoming data */
    uint8_t *srcPtr[3] = { img_data, img_data + 1, img_data + 2 };
    uint32_t srcSamplesPerPixel = 3;
    switch (vinfo.finfo->format) {
      case GST_VIDEO_FORMAT_RGBA:
        srcSamplesPerPixel = 4;
        break;
      case GST_VIDEO_FORMAT_BGRA:
        srcSamplesPerPixel = 4;
        srcPtr[0] = img_data + 2;
        srcPtr[1] = img_data + 1;
        srcPtr[2] = img_data + 0;
        break;
      case GST_VIDEO_FORMAT_ARGB:
        srcSamplesPerPixel = 4;
        srcPtr[0] = img_data + 1;
        srcPtr[1] = img_data + 2;
        srcPtr[2] = img_data + 3;
        break;
      case GST_VIDEO_FORMAT_ABGR:
        srcSamplesPerPixel = 4;
        srcPtr[0] = img_data + 3;
        srcPtr[1] = img_data + 2;
        srcPtr[2] = img_data + 1;
        break;
      case GST_VIDEO_FORMAT_BGR:
        srcPtr[0] = img_data + 2;
        srcPtr[1] = img_data + 1;
        srcPtr[2] = img_data + 0;
        break;
      default:
        break;
    }
    uint32_t stride = vinfo.stride[0];
    int32_t width = getWidth();
    int32_t height = getHeight();
    int32_t channels = getChannels();
    GstTensorDataType inputDatatype = getInputImageDatatype();
    const size_t inputTensorSize = width * height * channels * inputDatatypeSize;
    /* Get input tensor */
    int input = m_interpreter->inputs()[m_inputIndex];
    if (m_interpreter->AllocateTensors() != kTfLiteOk) {
      GST_ERROR_OBJECT (debug_parent, "Failed to allocate tensors!");
      return false;
    }
    switch (inputDatatype) {
      case GST_TENSOR_TYPE_UINT8:{
        uint8_t * dest = m_interpreter->typed_tensor<uint8_t>(input);
        if (dest == nullptr)
          return false;
        convert_image_remove_alpha (
          dest, m_inputImageFormat, srcPtr, srcSamplesPerPixel, stride,
          (uint8_t)m_inputStd, (uint8_t) 1);
        break;
      }
      case GST_TENSOR_TYPE_FLOAT32:{
        float * dest = m_interpreter->typed_tensor<float>(input);
        if (dest == nullptr)
          return false;
        convert_image_remove_alpha ((float*)dest, m_inputImageFormat , srcPtr,
          srcSamplesPerPixel, stride, (float)m_inputStd, m_inputStd);
        break;
      }
      default:{
        GST_ERROR_OBJECT (debug_parent, "Data type not handled");
        return false;
      }
      break;
    }

    /* Run inference */
    if (m_interpreter->Invoke() != kTfLiteOk) {
      GST_ERROR_OBJECT (debug_parent, "Failed to invoke tflite!");
      return false;
    }
    return true;
  }

  void GstTFliteClient::parseDimensions (GstVideoInfo vinfo)
  {
    bool fixedInputImageSize = isFixedInputImageSize();
    int32_t width = getWidth();
    int32_t height = getHeight();
    int32_t channels = getChannels();
    int32_t newWidth = fixedInputImageSize ? width : vinfo.width;
    int32_t newHeight = fixedInputImageSize ? height : vinfo.height;

    if (!fixedInputImageSize) {
      GST_WARNING_OBJECT (debug_parent, "Allocating before knowing model input size");
    }

    if (!dest || width * height < newWidth * newHeight) {
      delete[]dest;
      dest = new uint8_t[newWidth * newHeight * channels * inputDatatypeSize];
    }
    width = newWidth;
    height = newHeight;
  }

  template < typename T>
  void GstTFliteClient::convert_image_remove_alpha (T *dst,
      GstMlInputImageFormat hwc, uint8_t **srcPtr, uint32_t srcSamplesPerPixel,
      uint32_t stride, T std_value, T div) {
    size_t destIndex = 0;
    T tmp;

    int32_t width = getWidth();
    int32_t height = getHeight();
    int32_t channels = getChannels();

    if (hwc == GST_ML_INPUT_IMAGE_FORMAT_HWC) {
      for (int32_t j = 0; j < height; ++j) {
        for (int32_t i = 0; i < width; ++i) {
          for (int32_t k = 0; k < channels; ++k) {
            tmp = *srcPtr[k];
            tmp += std_value;
            dst[destIndex++] = (T)(tmp / div);
            srcPtr[k] += srcSamplesPerPixel;
          }
        }
        // correct for stride
        for (uint32_t k = 0; k < 3; ++k)
          srcPtr[k] += stride - srcSamplesPerPixel * width;
      }
    } else {
      size_t frameSize = width * height;
      T *destPtr[3] = { dst, dst + frameSize, dst + 2 * frameSize };
      for (int32_t j = 0; j < height; ++j) {
        for (int32_t i = 0; i < width; ++i) {
          for (int32_t k = 0; k < channels; ++k) {
            tmp = *srcPtr[k];
            tmp += std_value;
            destPtr[k][destIndex] = (T)(tmp / div);
            srcPtr[k] += srcSamplesPerPixel;
          }
          destIndex++;
        }
        // correct for stride
        for (uint32_t k = 0; k < 3; ++k)
          srcPtr[k] += stride - srcSamplesPerPixel * width;
      }
    }
  }
}
