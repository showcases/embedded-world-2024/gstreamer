/*
 * GStreamer gstreamer-tfliteinference
 * Copyright (C) 2024 Collabora Ltd.
 *
 * gsttfliteinference.cpp
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA 02110-1301, USA.
 */

/**
 * SECTION:element-tfliteinference
 * @short_description: Run TFLITE inference model on video buffers
 *
 * This element can apply an TFLITE model to video buffers. It attaches
 * the tensor output to the buffer as a @ref GstTensorMeta.
 *
 * To install TFLITE on your system, follow the instructions in the
 * README.md in with this plugin.
 *
 * ## Example launch command:
 *
 * Test image file, model file (SSD) and label file can be found here :
 * https://gitlab.collabora.com/gstreamer/tflite-models
 *
 * GST_DEBUG=ssdobjectdetector:5 \
 * gst-launch-1.0 filesrc location=tflite-models/images/bus.jpg ! \
 * jpegdec ! videoconvert ! tfliteinference execution-provider=cpu model-file=tflite-models/models/ssd_mobilenet_v1_coco.tflite !  \
 * ssdobjectdetector label-file=tflite-models/labels/COCO_classes.txt  ! videoconvert ! imagefreeze ! autovideosink
 *
 *
 * Note: in order for downstream tensor decoders to correctly parse the tensor
 * data in the GstTensorMeta, meta data must be attached to the TFLITE model
 * assigning a unique string id to each output layer. These unique string ids
 * and corresponding GQuark ids are currently stored in the tensor decoder's
 * header file, in this case gstssdobjectdetector.h. If the meta data is absent,
 * the pipeline will fail.
 *
 * As a convenience, there is a python script
 * currently stored at
 * https://gitlab.collabora.com/gstreamer/tflite-models/-/blob/master/scripts/modify_tflite_metadata.py
 * to enable users to easily add and remove meta data from json files. It can also dump
 * the names of all output layers, which can then be used to craft the json meta data file.
 *
 *
 */
#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <gst/gst.h>
#include "gsttfliteinference.h"
#include "gsttfliteclient.h"


/*
 * GstTFliteInference:
 *
 * @model_file model file
 * @execution_provider: TFLITE execution provider
 * @tflite_client opaque pointer to TFLITE client
 * @tflite_disabled true if inference is disabled
 * @video_info @ref GstVideoInfo of sink caps
 */
struct _GstTFliteInference
{
  GstBaseTransform basetransform;
  gchar *model_file;
  GstTFliteExecutionProvider execution_provider;
  bool ext_delegate_path_present = false;
  gpointer tflite_client;
  gboolean tflite_disabled;
  GstVideoInfo video_info;

  /* Stats */
  guint64 num_buffers;
  guint64 inf_time;
};

GST_DEBUG_CATEGORY (tflite_inference_debug);

#define GST_CAT_DEFAULT tflite_inference_debug
#define GST_TFLITE_CLIENT_MEMBER( self ) ((GstTFliteNamespace::GstTFliteClient *) (self->tflite_client))
GST_ELEMENT_REGISTER_DEFINE (tflite_inference, "tfliteinference",
    GST_RANK_PRIMARY, GST_TYPE_TFLITE_INFERENCE);

/* GstTFliteInference properties */
enum
{
  PROP_0,
  PROP_MODEL_FILE,
  PROP_INPUT_IMAGE_FORMAT,
  PROP_EXECUTION_PROVIDER,
  PROP_INPUT_INDEX,
  PROP_EXT_DELEGATE_LIB_PATH,
  PROP_THREADS,
  PROP_INPUT_STD,
  PROP_INPUT_MEAN,
  PROP_STATS
};

static GstStaticPadTemplate gst_tflite_inference_src_template =
GST_STATIC_PAD_TEMPLATE ("src",
    GST_PAD_SRC,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ RGB,RGBA,BGR,BGRA }"))
    );

static GstStaticPadTemplate gst_tflite_inference_sink_template =
GST_STATIC_PAD_TEMPLATE ("sink",
    GST_PAD_SINK,
    GST_PAD_ALWAYS,
    GST_STATIC_CAPS (GST_VIDEO_CAPS_MAKE ("{ RGB,RGBA,BGR,BGRA }"))
    );

static gboolean gst_tflite_inference_start (GstBaseTransform * trans);
static gboolean gst_tflite_inference_stop (GstBaseTransform * trans);

static void gst_tflite_inference_set_property (GObject * object,
    guint prop_id, const GValue * value, GParamSpec * pspec);
static void gst_tflite_inference_get_property (GObject * object,
    guint prop_id, GValue * value, GParamSpec * pspec);
static void gst_tflite_inference_finalize (GObject * object);
static GstFlowReturn gst_tflite_inference_transform_ip (GstBaseTransform *
    trans, GstBuffer * buf);
static gboolean gst_tflite_inference_process (GstBaseTransform * trans,
    GstBuffer * buf);
static GstCaps *gst_tflite_inference_transform_caps (GstBaseTransform *
    trans, GstPadDirection direction, GstCaps * caps, GstCaps * filter_caps);
static gboolean
gst_tflite_inference_set_caps (GstBaseTransform * trans, GstCaps * incaps,
    GstCaps * outcaps);

G_DEFINE_TYPE (GstTFliteInference, gst_tflite_inference,
    GST_TYPE_BASE_TRANSFORM);

GType gst_tflite_execution_provider_get_type (void);
#define GST_TYPE_TFLITE_EXECUTION_PROVIDER (gst_tflite_execution_provider_get_type ())

GType gst_ml_model_input_image_format_get_type (void);
#define GST_TYPE_ML_MODEL_INPUT_IMAGE_FORMAT (gst_ml_model_input_image_format_get_type ())

GType
gst_tflite_execution_provider_get_type (void)
{
  static GType tflite_execution_type = 0;

  if (g_once_init_enter (&tflite_execution_type)) {
    static GEnumValue execution_provider_types[] = {
      {GST_TFLITE_EXECUTION_PROVIDER_CPU, "CPU execution provider",
          "cpu"},
      {GST_TFLITE_EXECUTION_PROVIDER_NPU,
            "NPU execution provider",
          "npu"},
      {GST_TFLITE_EXECUTION_PROVIDER_EDGE_TPU,
            "Coral EdgeTPU acceleration",
          "coral"},
      {0, NULL, NULL},
    };

    GType temp = g_enum_register_static ("GstTFliteExecutionProvider",
        execution_provider_types);

    g_once_init_leave (&tflite_execution_type, temp);
  }

  return tflite_execution_type;
}

GType
gst_ml_model_input_image_format_get_type (void)
{
  static GType ml_model_input_image_format = 0;

  if (g_once_init_enter (&ml_model_input_image_format)) {
    static GEnumValue ml_model_input_image_format_types[] = {
      {GST_ML_INPUT_IMAGE_FORMAT_HWC,
            "Height Width Channel (HWC) a.k.a. interleaved image data format",
          "hwc"},
      {GST_ML_INPUT_IMAGE_FORMAT_CHW,
            "Channel Height Width (CHW) a.k.a. planar image data format",
          "chw"},
      {0, NULL, NULL},
    };

    GType temp = g_enum_register_static ("GstMlInputImageFormat",
        ml_model_input_image_format_types);

    g_once_init_leave (&ml_model_input_image_format, temp);
  }

  return ml_model_input_image_format;
}

static void
gst_tflite_inference_class_init (GstTFliteInferenceClass *klass)
{
  GObjectClass *gobject_class = (GObjectClass *) klass;
  GstElementClass *element_class = (GstElementClass *) klass;
  GstBaseTransformClass *basetransform_class = (GstBaseTransformClass *) klass;

  GST_DEBUG_CATEGORY_INIT (tflite_inference_debug, "tfliteinference",
      0, "tflite_inference");
  gobject_class->set_property = gst_tflite_inference_set_property;
  gobject_class->get_property = gst_tflite_inference_get_property;
  gobject_class->finalize = gst_tflite_inference_finalize;

  /**
   * GstTFliteInference:model-file
   *
   * TFLITE model file
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_MODEL_FILE,
      g_param_spec_string ("model-file",
          "TFLITE model file", "TFLITE model file", DEFAULT_MODEL_FILE, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));


  /**
   * GstTFliteInference:input-image-format
   *
   * Model input image format
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_IMAGE_FORMAT,
      g_param_spec_enum ("input-image-format",
          "Input image format",
          "Input image format",
          GST_TYPE_ML_MODEL_INPUT_IMAGE_FORMAT,
          DEFAULT_INPUT_IMAGE_FORMAT, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:execution-provider
   *
   * Execution provider
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_EXECUTION_PROVIDER,
      g_param_spec_enum ("execution-provider",
          "Execution provider",
          "TFLITE execution provider",
          GST_TYPE_TFLITE_EXECUTION_PROVIDER,
          DEFAULT_EXECUTION_PROVIDER, (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:model-file
   *
   * TFLITE External Delegate
   *
   * Since: 1.26
   */
  g_object_class_install_property (G_OBJECT_CLASS (klass), PROP_EXT_DELEGATE_LIB_PATH,
      g_param_spec_string ("ext-delegate",
          "External Delegate",
          "External Delegate library path for the NN inference", DEFAULT_EXT_DELEGATE_LIB_PATH,
          (GParamFlags)
          (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:execution-provider
   *
   * Number of Threads
   *
   * Since: 1.26
   */

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_THREADS,
      g_param_spec_int ("threads",
          "Number of Threads",
          "Set the number of threads to be used by the TFLITE inference",
          1, G_MAXINT, DEFAULT_THREADS,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:execution-provider
   *
   * Input tensor standard value
   *
   * Since: 1.26
   */

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_STD,
      g_param_spec_float ("input-std",
          "Input tensor standard value",
          "Standard value of the input",
          -G_MAXFLOAT, G_MAXFLOAT, DEFAULT_INPUT_STD,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:execution-provider
   *
   * Input mean
   *
   * Since: 1.26
   */

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_MEAN,
      g_param_spec_float ("input-mean",
          "Input mean",
          "Input tensor mean",
          -G_MAXFLOAT, G_MAXFLOAT, DEFAULT_INPUT_MEAN,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:stats:
   * Various statistics. This property returns a GstStructure
   * with name application/x-tflite-inference-stats with the following fields:
   *
   * <itemizedlist>
   * <listitem>
   *   <para>
   *   #guint64
   *   <classname>&quot;num-buffers&quot;</classname>:
   *   the number of buffers that has been processed.
   *   </para>
   * </listitem>
   * <listitem>
   *   <para>
   *   #guint64
   *   <classname>&quot;inf-time&quot;</classname>:
   *   the amount of time spent doing inference
   *   </para>
   * </listitem>
   * </itemizedlist>
   *
   * Since: 1.26
   */
  g_object_class_install_property (gobject_class, PROP_STATS,
      g_param_spec_boxed ("stats", "Statistics",
          "Statistics", GST_TYPE_STRUCTURE,
          (GParamFlags) (G_PARAM_READABLE | G_PARAM_STATIC_STRINGS)));

  /**
   * GstTFliteInference:input-index
   *
   * Tensor Input Index
   *
   * Since: 1.26
   */

  g_object_class_install_property (G_OBJECT_CLASS (klass),
      PROP_INPUT_INDEX,
      g_param_spec_int ("input-index",
          "Tensor Input Index",
          "Index of the input tensor, if more than one input tensor is present",
          0, G_MAXINT, DEFAULT_INPUT_INDEX,
          (GParamFlags) (G_PARAM_READWRITE | G_PARAM_STATIC_STRINGS)));

  gst_element_class_set_static_metadata (element_class, "tfliteinference",
      "Filter/Effect/Video",
      "Apply neural network to video frames and create tensor output",
      "Denis Shimizu <denis.shimizu@collabora.com>, Aaron Boxer <aaron.boxer@collabora.com>");
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_tflite_inference_sink_template));
  gst_element_class_add_pad_template (element_class,
      gst_static_pad_template_get (&gst_tflite_inference_src_template));
  basetransform_class->transform_ip =
      GST_DEBUG_FUNCPTR (gst_tflite_inference_transform_ip);
  basetransform_class->transform_caps =
      GST_DEBUG_FUNCPTR (gst_tflite_inference_transform_caps);
  basetransform_class->set_caps =
      GST_DEBUG_FUNCPTR (gst_tflite_inference_set_caps);
  basetransform_class->start = GST_DEBUG_FUNCPTR (gst_tflite_inference_start);
  basetransform_class->stop = GST_DEBUG_FUNCPTR (gst_tflite_inference_stop);
}

static void
gst_tflite_inference_init (GstTFliteInference *self)
{
  self->tflite_client =
      new GstTFliteNamespace::GstTFliteClient (GST_ELEMENT (self));
  self->tflite_disabled = TRUE;
}

static void
gst_tflite_inference_finalize (GObject *object)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (object);

  g_free (self->model_file);
  delete GST_TFLITE_CLIENT_MEMBER (self);
  G_OBJECT_CLASS (gst_tflite_inference_parent_class)->finalize (object);
}

static void
gst_tflite_inference_set_property (GObject *object, guint prop_id,
    const GValue *value, GParamSpec *pspec)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (object);
  const gchar *filename;
  auto tfliteClient = GST_TFLITE_CLIENT_MEMBER (self);

  switch (prop_id) {
    case PROP_MODEL_FILE:
      filename = g_value_get_string (value);
      if (filename
          && g_file_test (filename,
              (GFileTest) (G_FILE_TEST_EXISTS | G_FILE_TEST_IS_REGULAR))) {
        if (self->model_file)
          g_free (self->model_file);
        self->model_file = g_strdup (filename);
        self->tflite_disabled = FALSE;
      } else {
        GST_WARNING_OBJECT (self, "Model file '%s' not found!", filename);
      }
      break;
    case PROP_EXECUTION_PROVIDER:
      self->execution_provider =
          (GstTFliteExecutionProvider) g_value_get_enum (value);
      break;
    case PROP_INPUT_INDEX:
      tfliteClient->setInputIndex(g_value_get_int (value));
      break;
    case PROP_EXT_DELEGATE_LIB_PATH:
      filename = g_value_get_string (value);
      if (filename) {
        tfliteClient->setExtDelegatePath (filename);
        self->ext_delegate_path_present = true;
      } else {
        GST_WARNING_OBJECT (self, "Ext Delegate library file '%s' not found!",
            filename);
      }
      break;
    case PROP_INPUT_IMAGE_FORMAT:
      tfliteClient->setInputImageFormat ((GstMlInputImageFormat)
          g_value_get_enum (value));
      break;
    case PROP_THREADS:
      tfliteClient->setThreads (g_value_get_int (value));
      break;
    case PROP_INPUT_STD:
      tfliteClient->setInputStd (g_value_get_float (value));
      break;
    case PROP_INPUT_MEAN:
      tfliteClient->setInputMean (g_value_get_float (value));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static GstStructure *
gst_tflite_inference_create_stats (GstTFliteInference * self)
{
  GstStructure *s;

  GST_OBJECT_LOCK (self);
  s = gst_structure_new ("application/x-tflite-inference-stats",
      "inf-time", G_TYPE_UINT64, self->inf_time * G_GUINT64_CONSTANT (1000),
      "num-buffers", G_TYPE_UINT64, self->num_buffers, NULL);
  GST_OBJECT_UNLOCK (self);

  return s;
}

static void
gst_tflite_inference_get_property (GObject *object, guint prop_id,
    GValue *value, GParamSpec *pspec)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (object);
  auto tfliteClient = GST_TFLITE_CLIENT_MEMBER (self);

  switch (prop_id) {
    case PROP_MODEL_FILE:
      g_value_set_string (value, self->model_file);
      break;
    case PROP_EXECUTION_PROVIDER:
      g_value_set_enum (value, self->execution_provider);
      break;
    case PROP_INPUT_INDEX:
      g_value_set_int (value, tfliteClient->getInputIndex());
      break;
    case PROP_EXT_DELEGATE_LIB_PATH:
      g_value_set_string (value, tfliteClient->getExtDelegatePath ());
      break;
    case PROP_INPUT_IMAGE_FORMAT:
      g_value_set_enum (value, tfliteClient->getInputImageFormat ());
      break;
    case PROP_THREADS:
      g_value_set_int (value, tfliteClient->getThreads ());
      break;
    case PROP_INPUT_STD:
      g_value_set_float (value, tfliteClient->getInputStd ());
      break;
    case PROP_INPUT_MEAN:
      g_value_set_float (value, tfliteClient->getInputMean ());
      break;
    case PROP_STATS:
      g_value_take_boxed (value, gst_tflite_inference_create_stats (self));
      break;
    default:
      G_OBJECT_WARN_INVALID_PROPERTY_ID (object, prop_id, pspec);
      break;
  }
}

static gboolean
gst_tflite_inference_start (GstBaseTransform * trans)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (trans);
  auto tfliteClient = GST_TFLITE_CLIENT_MEMBER (self);
  gboolean ret = FALSE;

  GST_OBJECT_LOCK (self);
  if (tfliteClient->hasSession ()) {
    ret = TRUE;
    goto done;
  }

  if (self->model_file == NULL) {
    GST_ELEMENT_ERROR (self, STREAM, FAILED, (NULL),
		       ("model-file property not set"));
    goto done;
  }

  if ((self->ext_delegate_path_present == false) &&
      (self->execution_provider != GST_TFLITE_EXECUTION_PROVIDER_CPU)){
    GST_ELEMENT_ERROR (self, STREAM, FAILED, (NULL),
		       ("ext-delegate property not set"));
    goto done;
  }

  ret = GST_TFLITE_CLIENT_MEMBER (self)->createSession (self->model_file,
      self->execution_provider);

  if (!ret) {
    GST_ERROR_OBJECT (self,
        "Unable to create TFLITE session. Inference is disabled.");
  }

 done:
  GST_OBJECT_UNLOCK (self);

  return ret;
}

static gboolean
gst_tflite_inference_stop (GstBaseTransform * trans)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (trans);

  GST_OBJECT_LOCK (self);
  self->inf_time = 0;
  self->num_buffers = 0;
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}

static gboolean
gst_tflite_inference_has_session (GstTFliteInference * self)
{
  auto tfliteClient = GST_TFLITE_CLIENT_MEMBER (self);
  gboolean ret;

  GST_OBJECT_LOCK (self);
  ret = tfliteClient->hasSession ();
  GST_OBJECT_UNLOCK (self);

  return ret;
}

static GstCaps *
gst_tflite_inference_transform_caps (GstBaseTransform *trans,
    GstPadDirection direction, GstCaps *caps, GstCaps *filter_caps)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (trans);
  auto tfliteClient = GST_TFLITE_CLIENT_MEMBER (self);
  GstCaps *other_caps;
  GstCaps *restrictions;

  if (!gst_tflite_inference_has_session (self)) {
    other_caps = gst_caps_ref (caps);
    goto done;
  }

  restrictions = gst_caps_new_empty_simple ("video/x-raw");
  if (tfliteClient->isFixedInputImageSize ())
    gst_caps_set_simple (restrictions, "width", G_TYPE_INT,
        tfliteClient->getWidth (), "height", G_TYPE_INT,
        tfliteClient->getHeight (), NULL);

  if (tfliteClient->getInputImageDatatype () == GST_TENSOR_TYPE_UINT8 &&
      tfliteClient->getInputMean () == 1.0 &&
      tfliteClient->getInputStd () == 0.0) {
    switch (tfliteClient->getChannels ()) {
      case 1:
        gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "GRAY8",
            NULL);
        break;
      case 3:
        switch (tfliteClient->getInputImageFormat ()) {
          case GST_ML_INPUT_IMAGE_FORMAT_HWC:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGB",
                NULL);
            break;
          case GST_ML_INPUT_IMAGE_FORMAT_CHW:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBP",
                NULL);
            break;
        }
        break;
      case 4:
        switch (tfliteClient->getInputImageFormat ()) {
          case GST_ML_INPUT_IMAGE_FORMAT_HWC:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBA",
                NULL);
            break;
          case GST_ML_INPUT_IMAGE_FORMAT_CHW:
            gst_caps_set_simple (restrictions, "format", G_TYPE_STRING, "RGBAP",
                NULL);
            break;
        }
        break;
      default:
        GST_ERROR_OBJECT (self, "Invalid number of channels %d",
            tfliteClient->getChannels ());
        return NULL;
    }
  }

  GST_DEBUG_OBJECT (self, "Applying caps restrictions: %" GST_PTR_FORMAT,
      restrictions);

  other_caps = gst_caps_intersect_full (caps, restrictions,
      GST_CAPS_INTERSECT_FIRST);
  gst_caps_unref (restrictions);

 done:
  if (filter_caps) {
    GstCaps *tmp = gst_caps_intersect_full (other_caps, filter_caps,
        GST_CAPS_INTERSECT_FIRST);
    gst_caps_replace (&other_caps, tmp);
    gst_caps_unref (tmp);
  }

  return other_caps;
}

static gboolean
gst_tflite_inference_set_caps (GstBaseTransform *trans, GstCaps *incaps,
    GstCaps *outcaps)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (trans);
  auto tfliteClient = GST_TFLITE_CLIENT_MEMBER (self);

  if (!gst_video_info_from_caps (&self->video_info, incaps)) {
    GST_ERROR_OBJECT (self, "Failed to parse caps");
    return FALSE;
  }

  tfliteClient->parseDimensions (self->video_info);
  return TRUE;
}

static GstFlowReturn
gst_tflite_inference_transform_ip (GstBaseTransform *trans, GstBuffer *buf)
{
  if (!gst_base_transform_is_passthrough (trans)
      && !gst_tflite_inference_process (trans, buf)) {
    GST_ELEMENT_ERROR (trans, STREAM, FAILED,
        (NULL), ("TFLITE inference failed"));
    return GST_FLOW_ERROR;
  }

  return GST_FLOW_OK;
}

static gboolean
gst_tflite_inference_process (GstBaseTransform *trans, GstBuffer *buf)
{
  GstTFliteInference *self = GST_TFLITE_INFERENCE (trans);
  GstMapInfo info;
  guint64 start_time, end_time;

  start_time = g_get_monotonic_time ();

  if (gst_buffer_map (buf, &info, GST_MAP_READ)) {
    auto client = GST_TFLITE_CLIENT_MEMBER (self);
    if(! client->run (info.data, self->video_info))
      return FALSE;
    auto meta = client->copy_tensors_to_meta (buf);
    if (!meta)
      return FALSE;
    GST_TRACE_OBJECT (trans, "Num tensors:%d", meta->num_tensors);
    meta->batch_size = 1;
    gst_buffer_unmap (buf, &info);
  }

  end_time = g_get_monotonic_time ();

  GST_OBJECT_LOCK (self);
  self->num_buffers++;
  self->inf_time += (end_time - start_time);
  GST_OBJECT_UNLOCK (self);

  return TRUE;
}
